import sys

result = {}

for line in sys.stdin:
    line = line.strip()
    if len(line) == 0:
        continue
    key, vals = line.split(':')
    val = vals.split(',')
    result[key] = val

    if len(result) == 1:
        continue
    else:
        for i in result.get(key):
            for j in result:
                if i in result.get(j):
                    if j < key:
                        print(j+key, i)
                    elif key > j:
                        print(key+j, i)

