import sys

result = {}

for line in sys.stdin:
    line = line.strip()
    k,v = line.split(' ')
    if k in result:
        result.get(k).append(v)
    else:
        result[k] = [v]

for key, val in result.items():
    print(key, val)